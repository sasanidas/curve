;;; curve.el ---  curve            -*- lexical-binding: t; -*-

;; Copyright (C) 2020 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 11 Dec 2020
;; Version: 0.0.1
;; Keywords: extensions
;; URL: https://gitlab.com/sasanidas/curve.git
;; Package-Requires: ((emacs "26.3") (cl-lib "1.0"))
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires
(eval-when-compile
  (require 'cl-lib))

(require 'package)
(require 'seq)


(defgroup curve nil
  "Curve, past-generation extensible package manager."
  :prefix "curve-"
  :tag    "Curve"
  :group 'applications
  :link '(url-link :tag "Repository" "https://gitlab.com/sasanidas/curve.git"))


(defcustom curve-directory (expand-file-name ".curve" user-emacs-directory)
  "Curve main directory.
This is where all the important information about
packages and curve is stored."
  :group 'curve
  :type 'directory)


(defcustom curve-package-file (expand-file-name ".curve-packages.el" curve-directory)
  "File where curve store the package basic information.
Package name,version and dependencies."
  :group 'curve
  :type 'file)

(defcustom curve-package-directory-file (expand-file-name ".curve-directories.el"
						     curve-directory)
  "File where curve store the package directory information."
  :group 'curve
  :type 'file)

(defcustom curve-packages-source-directory (expand-file-name "source" curve-directory)
  "Directory where the source code packages are located."
  :group 'curve
  :type 'directory)

(defcustom curve-packages-build-directory (expand-file-name "build" curve-directory)
  "Directory where the build of the packages are located."
  :group 'curve
  :type 'directory)

(defcustom curve-packages-archives '((melpa . "https://melpa.org/")
				(emacsmirror . "https://github.com/emacsmirror"))
  "Remote places where the packages can be downloaded.
By default uses git as a download method."
  :group 'curve
  :type 'list)


;; '((pkg . (source . "version")) . '((pkg . "version" )... ))
(defvar curve-package-list
  '(((magit . (url . (git . "2.90.1"))) . ((emacs . "25.1")
					   (async . "20200113")
					   (git-commit . "20200516")
					   (transient . "20200601")))
    ((dash . (url . (git . "20200803"))) . ()))
  "List of package installed.
Defined in `curve-package-file' file.")

(defvar curve-pacakge-list-directory
  (make-hash-table :size (length curve-package-list))
  "Hash of the directories where the packages are installed.
Defined in `curve-package-directory-file'.")

;;'((git . ((:install . print))) (hg . ((:install . fncc))))
(defvar curve-package-source-types nil
  "List of the different packages types.
Defined with `curve-define-source'")


(cl-defun curve--package-name (&key package)
  "Syntax sugar for getting the PACKAGE version.
It receive a variable wit the standard assoc format
\(PKG . \(SOURCE . \"VERSION\"\) . \(...\)\)"
  (caar package))


(cl-defun curve--package-version (&key package)
  "Syntax sugar for getting the PACKAGE version.
It receive a variable wit the standard assoc format
\(PKG . \(SOURCE . \"VERSION\"\) . \(...\)\)"
  (format "%s"(cdddar package)))


(cl-defun curve--package-dependencies (&key package)
  "Syntax sugar for getting the PACKAGE dependencies.
It receive a variable wit the standard assoc format
\(PKG . \(SOURCE . \"VERSION\"\) . \(...\)\)"
  (cdr package))

(cl-defun curve--package-source (&key package)
  "Syntax sugar for getting the PACKAGE source.
It receive a variable wit the standard assoc format
\(PKG . \"VERSION\" . \(...\)\)"
  (caddar package))


(cl-defun curve--package-url (&key package)
  "Syntax sugar for getting the PACKAGE source.
It receive a variable wit the standard assoc format
\(PKG . \"VERSION\" . \(...\)\)"
  (caadar package))


(cl-defun curve--find-package (&key package-name)
  "Find a package with PACKAGE-NAME in `curve-package-list'."
  (seq-find (lambda (x)
	      (equal 'magit (caar x)))
	    curve-package-list))


(cl-defun curve--package-installed-p (&key package-name)
  "Check if PACKAGE-NAME is installed.
This mean that it's defined in the variable, `curve-package-list'
and in the file `curve-package-file'."
  (when (member package-name (mapcar #'caar curve-package-list))
    t))

;; '(((magit . (git . "2.90.1")) . ((emacs . "25.1")
;; 				(async . "20200113")
;; 				(git-commit . "20200516")
;; 				(transient . "20200601")))
;;  ((dash . (git . "20200803")) . ()))

(cl-defun curve--write-package-list (&key package-list)
  "Write PACKAGE-LIST into `curve-package-file'."
  (unless (file-directory-p curve-directory)
    (mkdir curve-directory))
  (with-temp-buffer
    (erase-buffer)
    (insert "(\n")
    (seq-map (lambda (package)
	       (insert (format "%s\n" package)))
	     package-list)
    (insert ")")
    (write-region (point-min) (point-max) curve-package-file)))

(cl-defun curve--read-package-list ()
  "Read the package list from `curve-package-file'.
It returns the list of packages."
  (if (file-exists-p curve-package-file)
      (with-temp-buffer
	(insert-file-contents curve-package-file)
	(read (buffer-string)))
    (error (format "The file %s doesn't exist."
		   curve-package-file))))


;; (curve-define-source 'hg '((:install . function)
;;  		      (:remove . function)
;;  		      (:update . function)
;;  		      (:check . function)))

;; (curve-define-source 'git '((:install . curve-git-install-package)
;;  		       (:remove . nil)
;;  		       (:update . nil)
;;  		       (:check . nil)))

(cl-defun curve-define-source (name &rest args)
  "Create a new source with NAME, and methods define in ARGS.
It also check if the source is define in
`curve-package-source-types'."
  (let* ((exist (seq-contains-p (mapcar #'car curve-package-source-types)
				name)))
    (if exist
	(error (format "The source %s is already define."
		       name))
      `(,name . ,args))))




(cl-defun curve--get-package-version (main-file)
  "Search for the version in MAIN-FILE.
Doesn't load the package and doesn't require the file
to be loaded."
  (with-temp-buffer
    (insert-file-contents-literally main-file)
    (lm-header "version")))



;; (curve-define-package :name 'magit :type 'git :source "https://github.com/magit/magit")
;;
;;


(cl-defun curve-define-package (&key name type source (version nil))
  "Define a new package.
This mean that, if the package is not installed, look for the
source TYPE in the `curve-package-source-types', if it is
defined, install the package with NAME from SOURCE.  SOURCE can
be an url, or any of the archives defines in
`curve-packages-archives'.If the source support vc,
it download the defined VERSION."
  ;;FIXME Check for the version of the package
  (if (member type (mapcar #'car curve-package-source-types))
      (let* ((package-installed? (curve--package-installed-p :package-name name))
	     (source-defined (assoc type curve-package-source-types))
	     (source-install (cdr (assoc :install (cdr source-defined))))
	     (source-version (cdr (assoc :check (cdr source-defined))))
	     (source-definition nil)
	     ;;
	     ;;
	     (install-version nil))

	(when (and package-installed?
		   version)
	  (setq install-version (funcall source-version (curve--find-package :package-name name)
					 version)))


	;; ((magit . (url . (git . "2.90.1"))) . ((emacs . "25.1")
	;; 					 (async . "20200113")
	;; 					 (git-commit . "20200516")
	;; 					 (transient . "20200601")))

	(setq source-definition (funcall source-install name type source install-version))

	(if source-definition
	    (push source-definition curve-package-list)
	  (error (format "Error defining the package %s."
			 name)))))
  (error (format "Package source %s is not define." type)))



					;
;; stable rx: (rx (+ (group (+ digit) (literal ".") (+ digit))))
;; roll rx: (rx (+ digit) eol)
;; (cl-defun curve--package-source-type (&key package)
;;   "Get the type of the source of the PACKAGE.
;; The types are:
;; stable-version: it refers to version dot, example 2.5.6
;; roll-version: MELPA format numbered version.
;; gi-hash: if there is none of the above, is a hash."
;;   (let* ((package-version
;; 	  (curve--package-version :package package)))
;;     (cond ((string-match-p "\\([[:digit:]]+\\.[[:digit:]]+\\)+" package-version)
;; 	   'stable-version)

;; 	  ;;FIXME But a regex with the MELPA year/month/day format
;; 	  ((string-match-p "[[:digit:]]+$" package-version)

;; 	   'roll-version)

;; 	  (t 
;; 	   'git-hash))))


;; (cl-defun curve-remove-package )

;; (cl-defun curve-purge-package )

;; (cl-defun curve-install-package )

;; (cl-defun curve-show-dependencies )





(provide 'curve)
;;; curve.el ends here
