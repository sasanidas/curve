;;; curve-git.el ---  curve-git            -*- lexical-binding: t; -*-

;; Copyright (C) 2020 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 12 Dec 2020
;; Version: 0.0.1
;; Keywords: extensions
;; URL: https://gitlab.com/sasanidas/curve.git
;; Package-Requires: ((emacs "26.3") (curve "0.0.1"))
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires
(eval-when-compile
  (require 'cl-lib))

(require 'package)
(require 'seq)
(require 'find-lisp)

(load "/home/fermin/Programming/curve/curve.el")

(defgroup curve-git nil
  "Curve git integration"
  :prefix "curve-git"
  :tag    "Curve-git"
  :group 'curve
  :link '(url-link :tag "Repository" "https://gitlab.com/sasanidas/curve.git"))

(defcustom curve-git-executable "git"
  "Curve-git, git executable location."
  :group 'curve-git
  :type 'string)

(defcustom curve-git-sources '(melpa)
  "Curve-git enable sources.
These are the source where curve-git know where to find
the packages."
  :group 'curve-git
  :type 'string)


(cl-defun curve-git-install-package (name source version)
  "Install a package with NAME.
It takes the package from SOURCE, which is this case
is from the TYPE git, if VERSION is non-nil it downloads
that branch of the package."
  (let* ((temp-directory (expand-file-name (make-temp-name
					    (concat name"-"))
					   (temporary-file-directory)))
	 (current-source (assoc source curve-packages-archives))
	 (package-directory (expand-file-name name temp-directory))
	 (package-dependencies nil)
	 (package-main-file nil)
	 (package-files nil))

    (mkdir temp-directory)
    (if (not (member (car current-source) curve-git-sources))
	(if version
	    (shell-command (format "cd %s && %s clone -b %s %s"
				   temp-directory
				   curve-git-executable version source))
	  (shell-command (format "cd %s && %s clone %s"
				 temp-directory curve-git-executable source)))

      (error (format "The source %s is not supported by curve-git." source)))

    (setq package-files (find-lisp-find-files package-directory "[^z-a]\\.el$"))

    (print package-files)

    (setq package-main-file (seq-find (lambda (pkg)
					(string-match (format "%s.el" name) pkg))
				      package-files))

    (print package-main-file)

    (with-temp-buffer
      (insert-file-contents package-main-file)
      (package-buffer-info))))



(cl-defun curve-git-enable ()
  "Enable the git backend for curve.
Defining a curve source and adding it to the variable
`curve-package-source-types'."
  (interactive)
  (add-to-list 'curve-package-source-types
	       (curve-define-source 'git '((:install . curve-git-install-package)
 					   (:remove . nil)
 					   (:update . nil)
 					   (:check . nil)))))


(cl-defun curve-git-disable ()
  "Disable the git backend for curve.
Remove it to the variable `curve-package-source-types'."
  (interactive)
  (setq curve-package-source-types (seq-filter (lambda (element)
						 (not (eq (car element) 'git)))
					       curve-package-source-types)))



(provide 'curve-git)
;;; curve-git.el ends here
