;;; test-curve.el --- curve test file.               -*- lexical-binding: t; -*-

;; Copyright (C) 20 Fermin Munoz

;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;; The requires
(require 'ert)
(load "/home/fermin/Programming/curve/curve.el")

(ert-deftest curve-package-list-test ()
  (should (listp curve-package-list)))


(ert-deftest curve-package-version-test ()
  (should (string= "2.90.1" (curve--package-version
			     :package'((magit . "2.90.1")
				       . ((emacs . "25.1")))))))

(ert-deftest curve-package-name-test ()
  (should (equal 'magit (curve--package-name
			 :package'((magit . "2.90.1")
				   . ((emacs . "25.1")))))))


(ert-deftest curve-package-dependencies-test ()
  (should (equal '((emacs . "25.1")
		   (async . "20200113"))
		 (curve--package-dependencies
		  :package '((magit . "2.90.1")
			     . ((emacs . "25.1")
				(async . "20200113")))))))



(provide 'test-curve.el)
;;; test-curve.el ends here
